package com.stackroute.datamunger;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

import com.stackroute.datamunger.query.DataTypeDefinitions;
import com.stackroute.datamunger.query.Header;
import com.stackroute.datamunger.reader.CsvQueryProcessor;

public class DataMunger {

	public static void main(String[] args) throws Exception{

		// read the file name from the user
		@SuppressWarnings("resource")
		Scanner scan = new Scanner(System.in);
		String fileName = scan.next();

		/*
		 * create object of CsvQueryProcessor. We are trying to read from a file inside
		 * the constructor of this class. Hence, we will have to handle exceptions.
		 */
		
		CsvQueryProcessor csvQueryProcessor = new CsvQueryProcessor(fileName);
		
		/*
		 * call getHeader() method of CsvQueryProcessor class to retrieve the array of
		 * headers
		 */
			csvQueryProcessor.getHeader();
		/*
		 * call getColumnType() method of CsvQueryProcessor class to retrieve the array
		 * of column data types which is actually the object of DataTypeDefinitions
		 * class
		 */
			csvQueryProcessor.getColumnType();
		/*
		 * display the columnName from the header object along with its data type from
		 * DataTypeDefinitions object
		 */

	}

}
