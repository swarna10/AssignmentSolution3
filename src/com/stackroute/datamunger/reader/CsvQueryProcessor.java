package com.stackroute.datamunger.reader;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.regex.Pattern;

import com.stackroute.datamunger.query.DataTypeDefinitions;
import com.stackroute.datamunger.query.Header;

public class CsvQueryProcessor extends QueryProcessingEngine {
	
	protected String fileName;
	BufferedReader bufferReader;

	// parameterized constructor to initialize filename
	public CsvQueryProcessor(String fileName) throws FileNotFoundException {
		this.fileName = fileName;
		bufferReader = new BufferedReader(new FileReader(this.fileName));		
	}

	/*
	 * implementation of getHeader() method. We will have to extract the headers
	 * from the first line of the file.
	 * Note: Return type of the method will be Header
	 */
	@Override
	public Header getHeader() throws IOException {
		// read the first line
		String header[] = null;
		bufferReader = new BufferedReader(new FileReader(this.fileName));
		header = bufferReader.readLine().toString().split(",");
		// populate the header object with the String array containing the header names
		Header headers = new Header();
		headers.setHeaders(header);
		headers.getHeaders();
		return headers;
	}

	/**
	 * getDataRow() method will be used in the upcoming assignments
	 */
	@Override
	public void getDataRow(){
		String line;
		try {
			bufferReader = new BufferedReader(new FileReader(this.fileName));
		} catch (FileNotFoundException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		LineNumberReader count = null;
		try {
			count = new LineNumberReader(new FileReader(this.fileName));
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		String dataRow[] = new String[count.getLineNumber()];
		int i = 0;
		try {
			while((line = bufferReader.readLine())!= null)
			{
				dataRow[i] = line;
				System.out.println(line);
				i++;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/*
	 * implementation of getColumnType() method. To find out the data types, we will
	 * read the first line from the file and extract the field values from it. If a
	 * specific field value can be converted to Integer, the data type of that field
	 * will contain "java.lang.Integer", otherwise if it can be converted to Double,
	 * then the data type of that field will contain "java.lang.Double", otherwise,
	 * the field is to be treated as String. 
	 * Note: Return Type of the method will be DataTypeDefinitions
	 */
	@Override
	public DataTypeDefinitions getColumnType() throws IOException {
		
		bufferReader = new BufferedReader(new FileReader(this.fileName));
		
		Pattern intCond = Pattern.compile("[0-9]+");
		Pattern doubleCond = Pattern.compile("[0-9][\\.][0-9]");
		
		String line = bufferReader.readLine();
		String dataRow = bufferReader.readLine();
		
		if(dataRow.endsWith(","))
		{
			dataRow = dataRow + " ";
		}
		
		String coloumnData[] = dataRow.split(",");
		String dataType[] = new String[coloumnData.length];
		int index =0;
		
		while(index < coloumnData.length)
		{
			
			if(intCond.matcher(coloumnData[index]).matches())
			{
				dataType[index] = "java.lang.Integer";
			}
			
			else if(doubleCond.matcher(coloumnData[index]).matches())
			{
				dataType[index] = "java.lang.Double";
			}
			else
			{
				dataType[index] = "java.lang.String";
			}
			index++;
		}
		
		DataTypeDefinitions dataTypeDefinitions = new DataTypeDefinitions();
		dataTypeDefinitions.setDataTypes(dataType);
		dataTypeDefinitions.getDataTypes();
		return dataTypeDefinitions;
	}
}
